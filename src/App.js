import './App.css';

// Method
const retornarAleatorio = () => Math.trunc(Math.random()*10)
const mostrarTitulo = title => (<h2>{title}</h2>)

function App() {
  // variables y constantes
  const siglo = 21
  const people = {
    name: 'Miguel',
    age: 30
  }
  const searchs = ['https://www.google.com', 'https://www.bing.com']
  
  return (
    <div className="App">
      <h1>Title H1</h1>
      <hr />
      {/* Interpolation */}
      <p>Estamos en el siglo: {siglo}</p>
      <br />
      <h3>Acceso a un objeto</h3>
      <p>{people.name} tiene {people.age} años.</p>
      <br />
      <h3>Llamando a un método</h3>
      {retornarAleatorio()}
      <br />
      <h3>Calculo de expresiones</h3>
      <p>3 + 3 = {3 + 3}</p>
      <div>
        <a href={searchs[0]}>Google</a><br />
        <a href={searchs[1]}>Bing</a><br />
      </div>
      <div>
        <p>{mostrarTitulo('Hi!')}</p>
        <p>{mostrarTitulo('Bye!')}</p>
      </div>
      <hr />
      {/* Form */}
      <div>
        <h2>Form</h2>
        <form onSubmit={presion}>
          <p>Ingrese el primer valor:
            <input type='number' name='valor1' />
          </p>
          <p>Ingrese el segundo valor:
            <input type='number' name='valor2' />
          </p>
          <p>
            <input type='submit' value='sumar' />
          </p>
        </form>
      </div>
    </div>
  );
}

const presion = e => {
  e.preventDefault();
  const v1=parseInt(e.target.valor1.value, 10);
  const v2=parseInt(e.target.valor2.value, 10);
  const suma = v1 + v2;
  alert(`La suma es: ${suma}`)
}

export default App;
